package com.daniel.controller;

import com.daniel.api.UserServiceApi;
import com.daniel.dto.UserInfo;
import com.daniel.support.BaseResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Autowired
    private UserServiceApi userService;

    @GetMapping("/getUserByID")
    public @ResponseBody BaseResult getUserByID(Long userID){
        UserInfo userInfo = userService.getUserInfoByID(userID);
        BaseResult<UserInfo> result = new BaseResult<UserInfo>();
        result.returnSuccessResult(userInfo);
        log.info("userInfo = ", userInfo);
        return result;
    }
}
