package com.daniel.api;

import com.daniel.dto.UserInfo;

public interface UserServiceApi {
    public UserInfo getUserInfoByID(Long userID);
}
