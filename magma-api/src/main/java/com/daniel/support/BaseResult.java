package com.daniel.support;

public class BaseResult<T> {
    private boolean success;
    private String errorMessage;
    private T data;

    public BaseResult() {
    }

    public void returnSuccessResult(T data){
        this.success = true;
        this.errorMessage = "success";
        this.data = data;
    }

    public void returnSuccessResultWithMessage(String errorMessage, T data){
        this.success = true;
        this.errorMessage = errorMessage;
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
